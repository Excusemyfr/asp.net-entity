﻿#region Using statements

using System;
using System.Linq;
using TranslationAssistant.Contracts.Managers;
using TranslationAssistant.Contracts.Repositories;
using TranslationAssistant.Contracts.Validators;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.BusinessLogic.Managers
{
    public class ProjectManager : IProjectManager
    {
        private readonly IRepository<Project> _repository;
        private readonly IValidator<Project> _validator;

        public ProjectManager(IRepository<Project> repository, IValidator<Project> validator)
        {
            _repository = repository;
            _validator = validator;
        }

        #region IProjectManager implementation

        public void Add(Project project)
        {
            if (!_validator.IsValid(project))
                return;

            _repository.Add(project);
            _repository.SaveChanges();
        }

        public void Update(Project project)
        {
            if (_validator.IsValid(project))
                _repository.SaveChanges();
        }

        public void Remove(Guid id)
        {
            var project = _repository.GetById(id);
            if (project == null)
                return;

            _repository.Remove(project);
            _repository.SaveChanges();
        }

        public Project GetProjectById(Guid id)
        {
            return _repository.GetById(id);
        }

        public IQueryable<Project> GetProjectsByUserId(Guid userId)
        {
            return _repository.GetAll().Where(p => p.UserId == userId);
        }

        #endregion
    }
}