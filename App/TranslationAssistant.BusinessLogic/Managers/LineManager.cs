﻿#region Using statements

using System;
using System.Linq;
using TranslationAssistant.Contracts.Managers;
using TranslationAssistant.Contracts.Repositories;
using TranslationAssistant.Contracts.Validators;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.BusinessLogic.Managers
{
    public class LineManager : ILineManager
    {
        private readonly IRepository<Line> _lineRepository;
        private readonly IValidator<Line> _validator;

        public LineManager(IRepository<Line> lineRepository, IValidator<Line> validator)
        {
            _lineRepository = lineRepository;
            _validator = validator;
        }

        #region ILineManager implementation

        public void Add(Line line)
        {
            if (!_validator.IsValid(line))
                return;

            _lineRepository.Add(line);
            _lineRepository.SaveChanges();
        }

        public void Update(Line line)
        {
            if (_validator.IsValid(line))
                _lineRepository.SaveChanges();
        }

        public void Remove(Guid id)
        {
            var line = _lineRepository.GetById(id);
            if (line == null)
                return;

            _lineRepository.Remove(line);
            _lineRepository.SaveChanges();
        }

        public Line GetLineById(Guid id)
        {
            return _lineRepository.GetById(id);
        }

        public IQueryable<Line> GetLinesByProjectId(Guid projectId)
        {
            return _lineRepository.Find(l => l.ProjectId == projectId);
        }

        public IQueryable<Line> GetLinesByUserId(Guid userId)
        {
            return _lineRepository.Find(l => l.Project.UserId == userId);
        }

        #endregion
    }
}