﻿#region Using statements

using System;
using TranslationAssistant.Contracts.Validators;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.BusinessLogic.Validators
{
    public class LineValidator : IValidator<Line>
    {
        public bool IsValid(Line entity)
        {
            return !string.IsNullOrEmpty(entity.Word) && (entity.ProjectId != Guid.Empty || entity.Project != null);
        }
    }
}