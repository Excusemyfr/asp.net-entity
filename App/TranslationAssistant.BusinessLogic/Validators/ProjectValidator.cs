﻿#region Using statements

using System;
using TranslationAssistant.Contracts.Validators;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.BusinessLogic.Validators
{
    public class ProjectValidator : IValidator<Project>
    {
        public bool IsValid(Project entity)
        {
            return !string.IsNullOrEmpty(entity.Title) && entity.UserId != Guid.Empty;
        }
    }
}