﻿#region Using statement

using System;
using System.Collections.Generic;
using Castle.Windsor;
using Microsoft.Practices.ServiceLocation;

#endregion

namespace TranslationAssistant.IoC.Castle
{
    public class WindsorServiceLocator : ServiceLocatorImplBase
    {
        private readonly IWindsorContainer _conteiner;

        public WindsorServiceLocator(IWindsorContainer conteiner)
        {
            _conteiner = conteiner;
        }

        protected override object DoGetInstance(Type serviceType, string key)
        {
            return key != null ? _conteiner.Resolve(key, serviceType) : _conteiner.Resolve(serviceType);
        }

        protected override IEnumerable<object> DoGetAllInstances(Type serviceType)
        {
            return (object[]) _conteiner.ResolveAll(serviceType);
        }
    }
}