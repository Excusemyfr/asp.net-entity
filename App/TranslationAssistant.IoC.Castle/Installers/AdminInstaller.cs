﻿#region Using statement

using System.Data.Entity;
using System.Web.Mvc;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Microsoft.Practices.ServiceLocation;
using TranslationAssistant.BusinessLogic.Managers;
using TranslationAssistant.BusinessLogic.Validators;
using TranslationAssistant.Contracts.Managers;
using TranslationAssistant.Contracts.Repositories;
using TranslationAssistant.Contracts.Validators;
using TranslationAssistant.Core;
using TranslationAssistant.Data.EF;
using TranslationAssistant.Data.EF.Repositories;

#endregion

namespace TranslationAssistant.IoC.Castle.Installers
{
    public class AdminInstaller : IWindsorInstaller
    {
        private const string WebAssemblyName = "TranslationAssistant.Web";

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyNamed(WebAssemblyName)
                .BasedOn<IController>()
                .LifestyleTransient()
                .Configure(x => x.Named(x.Implementation.FullName)));

            container.Register(
                Component.For<IWindsorContainer>().Instance(container),
                Component.For<WindsorControllerFactory>());

            #region DbContexts

            container.Register(Component.For<DbContext>()
                .ImplementedBy<DataContext>()
                .LifestyleSingleton());

            #endregion

            #region Repositories

            container.Register(Component.For(typeof (IRepository<>))
                .ImplementedBy(typeof (Repository<>))
                .LifestyleTransient());

            #endregion

            #region Validators

            container.Register(Component.For(typeof (IValidator<Project>))
                .ImplementedBy(typeof (ProjectValidator))
                .LifestylePerWebRequest());
            container.Register(Component.For(typeof (IValidator<Line>))
                .ImplementedBy(typeof (LineValidator))
                .LifestylePerWebRequest());

            #endregion

            #region Managers

            container.Register(Component.For(typeof (IProjectManager))
                .ImplementedBy(typeof (ProjectManager))
                .LifestylePerWebRequest());
            container.Register(Component.For(typeof (ILineManager))
                .ImplementedBy(typeof (LineManager))
                .LifestylePerWebRequest());

            #endregion

            var controllerFactory = new WindsorControllerFactory(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
            ServiceLocator.SetLocatorProvider(() => new WindsorServiceLocator(container));
        }
    }
}