﻿#region Using statement

using Microsoft.AspNet.Identity.EntityFramework;
using TranslationAssistant.Web.Core.Models;

#endregion

namespace TranslationAssistant.Web.Core.DbContext
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("TAConnection", throwIfV1Schema: false)
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}