﻿#region Using statements

using System;
using System.Data.Entity;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TranslationAssistant.Web.Core.DbContext;
using TranslationAssistant.Web.Core.Models;

#endregion

namespace TranslationAssistant.Web.Core.ApplicationDbInitializers
{
    public class DropCreateIfModelChangeApplicationInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        private static Guid GenerateGuidByCharacter(int value)
        {
            var tempString = string.Empty;

            for (var i = 0; i < 32; i++)
                tempString += value;

            return Guid.Parse(tempString);
        }

        protected override void Seed(ApplicationDbContext context)
        {
            const string userName = "username";

            if (!context.Users.Any(user => user.UserName == "Admin"))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser()
                {
                    Email = "god@god.god",
                    UserName = "god@god.god",
                    Name = "Vlad",
                    Surname = "Kholod"
                };

                manager.Create(user, "123qwe");
            }

            base.Seed(context);
        }
    }
}