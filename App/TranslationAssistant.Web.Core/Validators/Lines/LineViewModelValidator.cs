﻿#region Using statements

using FluentValidation;
using TranslationAssistant.Core;
using TranslationAssistant.Web.Core.Models.Lines;

#endregion

namespace TranslationAssistant.Web.Core.Validators.Lines
{
    public class LineViewModelValidator : AbstractValidator<LineViewModel>
    {
        public LineViewModelValidator()
        {
            RuleFor(l => l.Word)
                .NotEmpty().WithMessage("Word field is required.")
                .Length(1, 100).WithMessage("Word must be no longer than 100 characters")
                .WithName("Word");
        }
    }
}