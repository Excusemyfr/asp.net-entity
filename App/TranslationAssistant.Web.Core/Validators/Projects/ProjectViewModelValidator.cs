﻿#region Using statements

using System;
using FluentValidation;
using TranslationAssistant.Web.Core.Models.Projects;

#endregion

namespace TranslationAssistant.Web.Core.Validators.Projects
{
    public class ProjectViewModelValidator : AbstractValidator<ProjectViewModel>
    {
        public ProjectViewModelValidator()
        {
            RuleFor(p => p.Title)
                .NotEmpty().WithMessage("Project name is required.")
                .Length(10, 100).WithMessage("Project name must be at least 10 characters.")
                .WithName("Project name");
            RuleFor(p => p.CreateDate)
                .NotEmpty().WithMessage("Create date is required.")
                .LessThan(DateTime.Now).WithMessage("Create date can't be greater than just now.")
                .WithName("Create date");
        }
    }
}