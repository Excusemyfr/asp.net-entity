﻿#region Using statement

using System;
using FluentValidation.Attributes;
using TranslationAssistant.Web.Core.Validators.Projects;

#endregion

namespace TranslationAssistant.Web.Core.Models.Projects
{
    [Validator(typeof (ProjectViewModelValidator))]
    public class ProjectViewModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }
    }
}