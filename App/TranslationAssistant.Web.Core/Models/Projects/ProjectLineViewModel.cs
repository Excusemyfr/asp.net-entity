﻿#region Using statements

using System.Collections.Generic;
using TranslationAssistant.Core;
using TranslationAssistant.Web.Core.Models.Projects;

#endregion

namespace TranslationAssistant.Web.Core.Models.Projects
{
    public class ProjectLineViewModel : ProjectViewModel
    {
        public List<Line> Lines { get; set; }
    }
}