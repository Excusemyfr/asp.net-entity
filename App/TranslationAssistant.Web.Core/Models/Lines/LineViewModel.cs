﻿#region Using statements

using System;
using FluentValidation.Attributes;
using TranslationAssistant.Core;
using TranslationAssistant.Web.Core.Validators.Lines;

#endregion

namespace TranslationAssistant.Web.Core.Models.Lines
{
    [Validator(typeof (LineViewModelValidator))]
    public class LineViewModel
    {
        public Guid Id { get; set; }
        public string Word { get; set; }
        public string Description { get; set; }
        public Guid ProjectId { get; set; }
        public string ProjectTitle { get; set; }
    }
}