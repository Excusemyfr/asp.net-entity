﻿#region Using statement

using System.Data.Entity;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FluentValidation.Mvc;
using TranslationAssistant.Web.App_Start;
using TranslationAssistant.Web.Core.ApplicationDbInitializers;

#endregion

namespace TranslationAssistant.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Database.SetInitializer(new DropCreateIfModelChangeApplicationInitializer());

            var automapper = new AutoMapperConfig();
            automapper.Init();
            
            FluentValidationModelValidatorProvider.Configure();
        }
    }
}
