﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TranslationAssistant.Web.Startup))]
namespace TranslationAssistant.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
