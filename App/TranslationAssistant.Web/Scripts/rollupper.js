﻿function rollUp() {
    var $rollBox = $(".roll-box");

    if ($rollBox.height() < 45) {
        $rollBox.animate({ height: "237px" }, 500);
    } else {
        $rollBox.animate({height:"40px"},500);
    }
}

$(document).ready(function () {
    var $rollBox = $(".roll-box");

    $rollBox.css({
        overflow: "hidden"
    });

    $(".head-line").click(function () {
        rollUp();
    });
});