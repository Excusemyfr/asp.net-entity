﻿#region Using statements

using System.Web.Mvc;

#endregion

namespace TranslationAssistant.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
