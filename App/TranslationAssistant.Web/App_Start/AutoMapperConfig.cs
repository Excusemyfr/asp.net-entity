﻿#region Using statements

using AutoMapper;
using TranslationAssistant.Core;
using TranslationAssistant.Web.Core.Models.Lines;
using TranslationAssistant.Web.Core.Models.Projects;

#endregion

namespace TranslationAssistant.Web.App_Start
{
    public class AutoMapperConfig
    {
        public void Init()
        {
            #region Project

            Mapper.CreateMap<Project, ProjectViewModel>();
            Mapper.CreateMap<ProjectViewModel, Project>();

            Mapper.CreateMap<Project, ProjectLineViewModel>();
            Mapper.CreateMap<ProjectLineViewModel, Project>();

            #endregion

            #region Line

            Mapper.CreateMap<Line, LineViewModel>();
            Mapper.CreateMap<LineViewModel, Line>();

            #endregion
        }
    }
}