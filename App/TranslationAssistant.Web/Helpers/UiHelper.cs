﻿#region Using statements

using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;

#endregion

namespace TranslationAssistant.Web.Helpers
{
    public static class UiHelper
    {
        public static MvcHtmlString MenuLink(this HtmlHelper helper, string title, string action, string controller)
        {
            const string className = "active";

            var routeData = helper.ViewContext.RouteData.Values;
            var currentController = routeData["controller"];

            if (string.Equals(controller, currentController as string, StringComparison.OrdinalIgnoreCase))
            {
                return new MvcHtmlString("<li class=\"" + className + "\">" +
                                         helper.ActionLink(title, action, controller) +
                                         "</li>");
            }
            return new MvcHtmlString("<li>" + helper.ActionLink(title, action, controller) + "</li>");
        }
    }
}