﻿#region Using statements

using System;
using System.Web.Mvc;

#endregion

namespace TranslationAssistant.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}