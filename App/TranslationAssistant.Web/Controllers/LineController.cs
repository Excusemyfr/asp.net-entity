﻿#region Using statements

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using Microsoft.AspNet.Identity;
using TranslationAssistant.Contracts.Managers;
using TranslationAssistant.Core;
using TranslationAssistant.Web.Core.Models.Lines;

#endregion

namespace TranslationAssistant.Web.Controllers
{
    [Authorize]
    public class LineController : Controller
    {
        private readonly ILineManager _manager;

        public LineController(ILineManager manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var userGuid = Guid.Parse(User.Identity.GetUserId());

            var lines = _manager.GetLinesByUserId(userGuid);
            var lineViewModels = Mapper.Map<IQueryable<Line>, List<LineViewModel>>(lines);

            return View(lineViewModels);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return PartialView("_CreateLine");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LineViewModel model)
        {
            if (ModelState.IsValid)
            {
                var line = Mapper.Map<LineViewModel, Line>(model);
                _manager.Add(line);
            }

            if (HttpContext.Request.UrlReferrer != null)
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var line = _manager.GetLineById(id);
            var lineViewModel = Mapper.Map<Line, LineViewModel>(line);

            return View(lineViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid id, LineViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var line = _manager.GetLineById(id);
            line = Mapper.Map<LineViewModel, Line>(model, line);
            
            _manager.Update(line);

            return RedirectToAction("Edit", "Project", new RouteValueDictionary {{"id", model.ProjectId}});
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            _manager.Remove(id);

            if (HttpContext.Request.UrlReferrer != null)
                return Redirect(HttpContext.Request.UrlReferrer.AbsoluteUri);

            return RedirectToAction("Index");
        }
    }
}