﻿#region Using statements

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Microsoft.AspNet.Identity;
using TranslationAssistant.Contracts.Managers;
using TranslationAssistant.Core;
using TranslationAssistant.Web.Core.Models.Projects;

#endregion

namespace TranslationAssistant.Web.Controllers
{
    [Authorize]
    public class ProjectController : Controller
    {
        private readonly IProjectManager _manager;

        public ProjectController(IProjectManager manager)
        {
            _manager = manager;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var userGuid = Guid.Parse(User.Identity.GetUserId());

            var projects = _manager.GetProjectsByUserId(userGuid);
            var projectViewModels = Mapper.Map<IQueryable<Project>, List<ProjectViewModel>>(projects);

            return View(projectViewModels);
        }

        [HttpGet]
        public ActionResult Details(Guid id)
        {
            var project = _manager.GetProjectById(id);
            var projectLineViewModel = Mapper.Map<Project, ProjectLineViewModel>(project);

            return View(projectLineViewModel);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProjectViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var project = Mapper.Map<ProjectViewModel, Project>(model);
            project.UserId = Guid.Parse(User.Identity.GetUserId());

            _manager.Add(project);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var project = _manager.GetProjectById(id);
            var projectLineViewModel = Mapper.Map<Project, ProjectLineViewModel>(project);

            return View(projectLineViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid id, ProjectLineViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var project = _manager.GetProjectById(id);
            project = Mapper.Map<ProjectViewModel, Project>(model, project);

            _manager.Update(project);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            _manager.Remove(id);

            return RedirectToAction("Index");
        }
    }
}