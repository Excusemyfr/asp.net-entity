﻿#region Using statements

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.Data.EF.Mappings
{
    public class LineMap : EntityTypeConfiguration<Line>
    {
        public LineMap()
        {
            this.HasKey(l => l.Id);
            this.Property(l => l.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(l => l.Word)
                .HasMaxLength(100)
                .IsRequired();
            this.Property(l => l.Description)
                .HasMaxLength(255)
                .IsOptional();
            this.Property(l => l.ProjectId)
                .IsRequired();

            this.HasRequired(l => l.Project)
                .WithMany(p => p.Lines)
                .HasForeignKey(l => l.ProjectId)
                .WillCascadeOnDelete(true);

            this.ToTable("Line");
        }
    }
}