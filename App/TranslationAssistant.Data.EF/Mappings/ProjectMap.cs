﻿#region Using statements

using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.Data.EF.Mappings
{
    public class ProjectMap : EntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            this.HasKey(p => p.Id);
            this.Property(p => p.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(p => p.Title)
                .HasMaxLength(100)
                .IsRequired();
            this.Property(p => p.CreateDate)
                .IsRequired();
            this.Property(p => p.UserId)
                .IsRequired();

            this.ToTable("Project");
        }
    }
}