﻿#region Using statement

using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using TranslationAssistant.Contracts.Repositories;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.Data.EF.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbContext _context;
        private readonly IDbSet<T> _entities;

        public Repository(DbContext context)
        {
            _context = context;
            _entities = context.Set<T>();
        }

        #region IRepository<T> implementation

        public void Add(T entity)
        {
            _entities.Add(entity);
        }

        public void Remove(T entity)
        {
            _entities.Remove(entity);
        }

        public T GetByGuid(Guid guid)
        {
            return _entities.FirstOrDefault(e => e.Id == guid);
        }

        public T GetById(object id)
        {
            return _entities.Find(id);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _entities.Where(expression);
        }

        public IQueryable<T> GetAll()
        {
            return _entities;
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        #endregion
    }
}