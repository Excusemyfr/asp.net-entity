﻿#region Using statement

using System.Data.Entity;
using TranslationAssistant.Core;
using TranslationAssistant.Data.EF.Mappings;

#endregion

namespace TranslationAssistant.Data.EF
{
    public class DataContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Line> Lines { get; set; }

        public DataContext()
            : base("TAConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProjectMap());
            modelBuilder.Configurations.Add(new LineMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}