#region Using statement

using System.Data.Entity.Migrations;

#endregion

namespace TranslationAssistant.Data.EF.Migrations
{
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Line",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Word = c.String(nullable: false, maxLength: 100),
                    Description = c.String(maxLength: 255),
                    ProjectId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Project", t => t.ProjectId, cascadeDelete: true)
                .Index(t => t.ProjectId);

            CreateTable(
                "dbo.Project",
                c => new
                {
                    Id = c.Guid(nullable: false, identity: true),
                    Title = c.String(nullable: false, maxLength: 100),
                    CreateDate = c.DateTime(nullable: false),
                    UserId = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.Id);
        }

        public override void Down()
        {
            DropForeignKey("dbo.Line", "ProjectId", "dbo.Project");
            DropIndex("dbo.Line", new[] {"ProjectId"});
            DropTable("dbo.Project");
            DropTable("dbo.Line");
        }
    }
}