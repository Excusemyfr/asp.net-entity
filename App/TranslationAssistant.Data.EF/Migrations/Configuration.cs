#region Using statement

using System.Data.Entity.Migrations;

#endregion

namespace TranslationAssistant.Data.EF.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(DataContext context)
        {
        }
    }
}