﻿#region Using statement

using System;
using System.Collections.Generic;

#endregion

namespace TranslationAssistant.Core
{
    public class Project : BaseEntity
    {
        public string Title { get; set; }
        public DateTime CreateDate { get; set; }
        public Guid UserId { get; set; }

        public virtual List<Line> Lines { get; set; }

        public Project()
        {
            Lines = new List<Line>();
        }
    }
}