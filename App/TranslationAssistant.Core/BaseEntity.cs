﻿#region Using statement 

using System;

#endregion

namespace TranslationAssistant.Core
{
    public class BaseEntity
    {
        public Guid Id { get; protected set; }
    }
}