﻿#region Using statement

using System;

#endregion

namespace TranslationAssistant.Core
{
    public class Line : BaseEntity
    {
        public string Word { get; set; }
        public string Description { get; set; }
        public Guid ProjectId { get; set; }

        public virtual Project Project { get; set; }
    }
}