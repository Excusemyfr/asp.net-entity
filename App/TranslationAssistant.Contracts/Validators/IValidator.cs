﻿#region Using statements

using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.Contracts.Validators
{
    public interface IValidator<in T> where T : BaseEntity
    {
        bool IsValid(T entity);
    }
}