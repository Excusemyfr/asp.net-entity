﻿#region Using statements

using System;
using System.Linq;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.Contracts.Managers
{
    public interface IProjectManager
    {
        void Add(Project project);
        void Update(Project project);
        void Remove(Guid id);
        Project GetProjectById(Guid id);
        IQueryable<Project> GetProjectsByUserId(Guid userId);
    }
}