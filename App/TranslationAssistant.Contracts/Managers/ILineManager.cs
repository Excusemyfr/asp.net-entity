﻿#region Using statements

using System;
using System.Linq;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.Contracts.Managers
{
    public interface ILineManager
    {
        void Add(Line line);
        void Update(Line line);
        void Remove(Guid id);
        Line GetLineById(Guid id);
        IQueryable<Line> GetLinesByProjectId(Guid projectId);
        IQueryable<Line> GetLinesByUserId(Guid userId);
    }
}