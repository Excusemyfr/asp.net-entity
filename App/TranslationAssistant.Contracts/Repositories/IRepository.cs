﻿#region Using statement

using System;
using System.Linq;
using System.Linq.Expressions;
using TranslationAssistant.Core;

#endregion

namespace TranslationAssistant.Contracts.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Add(T entity);
        void Remove(T entity);
        T GetById(object id);
        IQueryable<T> Find(Expression<Func<T, bool>> expression);
        IQueryable<T> GetAll();
        void SaveChanges();
    }
}